## merlin-user 10 QP1A.190711.020 V11.0.2.0.QJOEUXM release-keys
- Manufacturer: xiaomi
- Platform: mt6768
- Codename: merlin
- Brand: Redmi
- Flavor: lineage_merlin-userdebug
- Release Version: 11
- Id: RQ3A.210605.005
- Incremental: eng.pizza.20210620.151709
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Redmi/merlin_eea/merlin:10/QP1A.190711.020/V11.0.2.0.QJOEUXM:user/release-keys
- OTA version: 
- Branch: merlin-user-10-QP1A.190711.020-V11.0.2.0.QJOEUXM-release-keys-random-text-189913269531040
- Repo: redmi_merlin_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
